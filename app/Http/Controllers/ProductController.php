<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use DB;

class ProductController extends Controller
{
    public function createProduct(Request $request){
        $product = Product::create($request->all());

        return response()->json($product);
    }

    public function getProducts(){
        $products = Product::all();
        return response()->json($products);
    }

    public function getProductsByCategory($category){
        $products = Product::where('category', $category)->get();
        return response()->json($products);
    }
}
