<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\product_order::class, function (Faker $faker) {
    $order = sprintf('%s', $faker->numberBetween($min = 1, $max = 10));
        // $faker->randomElement([
        //     '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'
        // ]));
        $product = sprintf('%s', $faker->numberBetween($min = 2, $max = 51));
        // $faker->randomElement([
        //     '2', '3', '4', '5', '6', '7', '8', '9', '10',
        //     '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
        //     '21', '22', '23', '24', '25', '26', '27', '28', '29', '30',
        //     '31', '32', '33', '34', '35', '36', '37', '38', '39', '40',
        //     '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51'
        // ]));
    return [
        // 'order_id'-> $faker->numberBetween($min = 1, $max = 10),
        // 'product_id'-> $faker->numberBetween($min = 2, $max = 51),
        'order_id'-> $order,
        'product_id'-> $product,
    ];
});
