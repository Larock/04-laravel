<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement([
            'Piet',
            'Jan',
            'Bram',
            'Niko',
            'Fabienne',
            'Lotte',
            'Madeleine',
            'Miguel',
            'Gilles',
            'Honorine',
            'Simon',
            'Quinten',
            'Erik',
            'Hendrik',
            'Seppe',
            'Nathan',
            'Fleur',
            'Niels',
            'Karen',
            'Elise',
            'Luc',
            'Judith',
            'Kas',
            'Arnaud',
            'Lies',
            'Elien',
            'Amber',
            'Jeroen',
            'Roos',
            'Ward'
        ]),
        'email' => $faker->unique()->safeEmail,
        'status'=> $faker->randomElement([
            'Request send'
        ]),
    ];
});
