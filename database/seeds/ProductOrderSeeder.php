<?php

use Illuminate\Database\Seeder;
use App\Order;

class ProductOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory('App\product_order', 2)->create();
        $orderid = rand(1,30);
        $product1 = rand(2,16);
        $product2 = rand(17, 31);
        $product3 = rand(32, 51);
        $productids = [$product1, $product2, $product3];

        $order = Order::find($orderid);
        
        $order->products()->sync($productids);
    }
}
